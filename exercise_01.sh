#! /bin/bash
# Write a shell script that prompts the user for a name of a file or directory [[passed]]
# and reports if it is a regular file, a directory, or another type of file.	[[]]
# Also perform an ls command against the file or directory with the long listing option.

read -p "name of a file or directory " name
if [[ -d $name ]]; then
	echo "$name is a directory"
	ls -ls $name
elif [[ -f $name ]]; then
	echo "$name is a file"
	ls -ls $name
else
	echo "$name is not valid or not exists!"
fi
